// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import './assets/style/base_style.less'
import common from './common/common'
import 'lib-flexible'
import 'babel-polyfill'
import Es6Promise from "es6-promise"
Es6Promise.polyfill();
import "swiper/dist/css/swiper.min.css"


import VueTouch from 'vue-touch'
Vue.use(VueTouch, {name: 'v-touch'});
VueTouch.config.swipe = {
  threshold: 100 //手指左右滑动距离
};


import Element from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(Element);
/*如需使用jquery，把下边注释放开，并且把webpack.base.config.js里关于jquery的注释放开*/
// import $ from "jquery"

/*以下方法都添加到了Vue原型链上，在.vue文件中调用的时候千万别忘了加this如果这样使用着不习惯，也可以自行把这些方法添加到window对象上，调用的时候直接调用，不用加this*/

/*截取客户端拼接在url上的参数。注意！调用该方法传参数为字符串,如：getParamsFromUrl("imei")，返回值为截取到的imei的值，如果客户端没有传这个参数，返回值为空""*/
Vue.prototype.getParamsFromUrl = common.getParamsFromUrl;
/*判断设备是android(返回值为1)还是ios(返回值为2)*/
Vue.prototype.isMobile = common.isMobile;
/*主要用来注册H5方法供客户端调用，如需调用客户端方法请使用下边封装好的callAppMethod*/
Vue.prototype.setupWebViewJavascriptBridge = common.setupWebViewJavascriptBridge;
/*调用客户端的方法，三个参数，其中二，三参数可以不传，如：callAppMethod({
callName:"lastGoBack",
parameters:{
name:"lisong",
age:"30"
},
callback:function(res){
if(res){
console.log("接收到了客户端传过来的数据")
}
}
})*/
Vue.prototype.callAppMethod = common.callAppMethod;
/*格式化金额。每三位以“，”隔开，第一个参数为金额，第二个参数为小数点以后保留的位数*/
Vue.prototype.fMoney = common.fMoney;

Vue.filter("numFilter",(num)=>num < 10 ? `0${num}` : `${num}`);

Vue.config.productionTip = false;
/*路由守卫*/
router.beforeEach((to,from,next)=>{
  if(to.meta.title){
    document.title = to.meta.title
  }else{
    document.title = "全局搜索关键词维护"
  }
  next();
});
router.afterEach((to,from,next)=>{

});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
