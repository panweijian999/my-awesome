import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex);
import axios from "axios"
import qs from "qs"
import common from "../common/common"

/*与后台交互时设置Content-Type，如果后台需要其他类型可修改为其他类型，自行百度*/
axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";
axios.defaults.withCredentials = true;
/*设置接口的公共部分*/
// axios.defaults.baseURL = process.env.NODE_ENV === "development" ? "http://22.11.235.15:8080/mlifeWeb" : "/mlifeWeb";//罗海平本机
axios.defaults.baseURL = process.env.NODE_ENV === "development" ? "http://22.11.235.20:8080/mlifeWeb" : "/mlifeWeb";//206
// axios.defaults.baseURL = "/mlifeWeb";
/*封装的ajax方法*/
const ajaxFn = (url,parameters = {},fn = "post")=>{
  if(fn === "post"){
  return new Promise((resolve,reject)=>{
    axios
      .post(url,qs.stringify(parameters))
      .then(res=>{
        resolve(res)
      })
      .catch(err=>{
        reject(err)
      })
  })
  }else {
    return new Promise((resolve,reject)=>{
      axios
        .get(url,qs.stringify(parameters))
        .then(res=>{
          resolve(res)
        })
        .catch(err=>{
          reject(err)
        })
    })
  }
};

const state = {
  bgTransparentShow:false,
  bgGrayShow: false,
  alertBoxShow: false,
  testState01: "store文件里的state",
  alertTitle: "",
  alertContent: "",
  cancelText: "",
  confirmText: "",
  confirmStat: "",
  loadingShow: false,
  header_left_img: "",
  header_middle_text: "测试头部标题",
  header_right_img: "",
};
const mutations = {
  /*触发弹框的方法*/
  show_alert_box(state, val) {
    state.alertBoxShow = true;
    state.bgGrayShow = true;
    state.alertTitle = val.alertTitle;
    state.alertContent = val.alertContent;
    state.cancelText = val.cancelText;
    state.confirmText = val.confirmText;
    state.confirmStat = val.confirmStat;
    document.body.style.overflow = "hidden"
  },
  /*点击取消执行的方法。点击确定执行的回调统一在App.vue文件处理*/
  alert_cancel(state) {
    state.alertBoxShow = false;
    state.bgGrayShow = false;
    document.body.style.overflow = "visible"
  },
  /*设置公共头部的方法*/
  set_header(state, val) {
    state.header_left_img = val.header_left_img;
    state.header_middle_text = val.header_middle_text;
    state.header_right_img = val.header_right_img;
  }
};
const getters = {};
const actions = {
  findRange(context){
		return ajaxFn("/range/findRange.do",{},"get")
	},
	find(context,postData){
		return ajaxFn("/keyword/find.do",postData)
	},
	update(context,postData){
		return ajaxFn("/keyword/update.do",postData)
  },
  insertKeyWord(context, postData) {
    return ajaxFn("/keyword/insertKeyWord.do", postData);
  },
  findAllRange(context) {
    return ajaxFn("/range/findAllRange.do", {}, "get");
  }
};
axios.interceptors.request.use(config => {
	// console.log(config)
  state.loadingShow = true;
  state.bgTransparentShow = true;
  document.body.style.overflow = "hidden";
  return config
});
axios.interceptors.response.use(response => {
  // console.log(response)
  if(response.status === 200){
    state.loadingShow = false;
    state.bgTransparentShow = false;
    document.body.style.overflow = "visible";
		if(response.data.list){
			return response.data
		}
    if(response.data.stat === "00"){
			if(response.data.body){
				return response.data.body
			}else{
				return response.data.stat
			}
    }else if(response.data.stat === "01" || response.data.stat === "-501"){
      mutations.show_alert_box(state,{
        alertTitle:"",
        alertContent:"您已经超时，请重新登录",
        cancelText:"",
        confirmText:"确定",
        confirmStat:"登录超时"
      });
    }else if(response.data.stat === "02"){
      mutations.show_alert_box(state,{
        alertTitle:"",
        alertContent:"发现您的账户在其他设备登录，如非本人操作请尽快更改您的登录密码",
        cancelText:"",
        confirmText:"确定",
        confirmStat:"异地登录"
      });
    }else if(response.data.stat === "-505"){
      mutations.show_alert_box(state,{
        alertTitle:"",
        alertContent:response.data.result,
        cancelText:"",
        confirmText:"确定",
        confirmStat:"设备号异地校验失败"
      });
    }else{
      mutations.show_alert_box(state,{
        alertTitle:"",
        alertContent:response.data.result ,
        cancelText:"",
        confirmText:"确定",
        confirmStat:"01"
      });
    }
  }else {
    mutations.show_alert_box(state,{
      alertTitle:"",
      alertContent:"系统错误，请稍后重试！",
      cancelText:"",
      confirmText:"确定",
      confirmStat:"back"
    });
  }
  // console.log(response)
  return response
  // return Promise.resolve(response)
}, error => {
  console.log(error);
  // console.log(error.response);
  if(error.response){
    if(error.response.status === 404){
      mutations.show_alert_box(state,{
        alertTitle:"",
        alertContent:"连接服务器出错，请稍后重试！",
        cancelText:"",
        confirmText:"知道了",
        confirmStat:"01"
      });
      state.loadingShow = false;
      state.bgTransparentShow = false;
    }
  }
  return Promise.reject(error)
});
export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})
