function getParamsFromUrl(arg = "") {
  let url = window.location.href;
  let paramsString = url.split("?")[1];
  paramsString = paramsString.indexOf("#/") > -1 ? paramsString.replace(/#\//g, "") : paramsString;
  let paramsObj = {};
  paramsString.split("&").forEach(function (item, index) {
    paramsObj[item.split("=")[0]] = item.split("=")[1]
  });
  if (arg && paramsObj[arg]) {
    return paramsObj[arg]
  } else {
    console.log("查询的参数不存在")
    return ""
  }
}

function isMobile() {
  if (/android/i.test(navigator.userAgent) || /adr/i.test(navigator.userAgent)) {
    return 1
  } else if (/iPhone|iPad|iPod|iOS/i.test(navigator.userAgent)) {
    return 2
  } else {
    return 1
  }
}

function setupWebViewJavascriptBridge(callback) {
  if (window.WebViewJavascriptBridge) {
    callback(WebViewJavascriptBridge);
  } else {
    document.addEventListener(
      'WebViewJavascriptBridgeReady',
      function () {
        callback(WebViewJavascriptBridge);
      }, false);
  }
  if (window.WVJBCallbacks) {
    return window.WVJBCallbacks.push(callback);
  }
  window.WVJBCallbacks = [callback];
  var WVJBIframe = document.createElement('iframe');
  WVJBIframe.style.display = 'none';
  WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
  document.documentElement.appendChild(WVJBIframe);
  setTimeout(function () {
    document.documentElement.removeChild(WVJBIframe);
  }, 0);
}

function callAppMethod(obj) {
  let callName = obj.callName || "";
  let parameters = obj.parameters || {};
  let callback = obj.callback || function (data) {
  };
  if (!callName) {
    return
  } else {
    setupWebViewJavascriptBridge(function (bridge) {
      bridge.callHandler(callName, parameters, callback)
    })
  }
}

function currencySymbol(item) {
  if (item === "USD" || item === "USD" || item === "ARS" || item === "AUD" || item === "BSD" || item === "BBD" || item === "BMD" || item === "BND" || item === "CAD" || item === "KYD" || item === "CLP" || item === "COP" || item === "XCD" || item === "SVC" || item === "FJD" || item === "GYD" || item === "LRD" || item === "MXN" || item === "NAD" || item === "NZD" || item === "SGD" || item === "SBD" || item === "SRD" || item === "TVD") {
    return "$"
  } else if (item === "AFN") {
    return "؋"
  } else if (item === "AWG") {
    return "ƒ"
  } else if (item === "AZN") {
    return "ман"
  } else if (item === "BYR") {
    return "p."
  } else if (item === "EUR") {
    return "€"
  } else if (item === "BZD") {
    return "BZ$"
  } else if (item === "BOB") {
    return "$b"
  } else if (item === "BAM") {
    return "KM"
  } else if (item === "BWP") {
    return "P"
  } else if (item === "BGN") {
    return "лв"
  } else if (item === "BRL") {
    return "R$"
  } else if (item === "GBP") {
    return "£"
  } else if (item === "KHR") {
    return "៛"
  } else if (item === "CNY") {
    return "¥"
  } else if (item === "CRC") {
    return "₡"
  } else if (item === "HRK") {
    return "kn"
  } else if (item === "CUP") {
    return "₱"
  } else if (item === "CZK") {
    return "Kč"
  } else if (item === "DKK") {
    return "kr"
  } else if (item === "DOP") {
    return "RD$"
  } else if (item === "EGP") {
    return "£"
  } else if (item === "GBP") {
    return "£"
  } else if (item === "EEK") {
    return "kr"
  } else if (item === "FKP") {
    return "£"
  } else if (item === "GHC") {
    return "¢"
  } else if (item === "GIP") {
    return "£"
  } else if (item === "GTQ") {
    return "Q"
  } else if (item === "GGP") {
    return "£"
  } else if (item === "GYD") {
    return "؋"
  } else if (item === "HNL") {
    return "L"
  } else if (item === "HKD") {
    return "HK$"
  } else if (item === "HUF") {
    return "Ft"
  } else if (item === "ISK") {
    return "kr"
  } else if (item === "INR") {
    return "₨"
  } else if (item === "IDR") {
    return "Rp"
  } else if (item === "IRR") {
    return "﷼"
  } else if (item === "IMP") {
    return "£"
  } else if (item === "ILS") {
    return "₪"
  } else if (item === "JMD") {
    return "J$"
  } else if (item === "JPY") {
    return "¥"
  } else if (item === "JEP") {
    return "£"
  } else if (item === "KZT") {
    return "лв"
  } else if (item === "KPW" || item === "KRW") {
    return "₩"
  } else if (item === "KGS") {
    return "лв"
  } else if (item === "LAK") {
    return "₭"
  } else if (item === "LVL") {
    return "Ls"
  } else if (item === "LBP") {
    return "£"
  } else if (item === "CHF") {
    return "CHF"
  } else if (item === "LTL") {
    return "Lt"
  } else if (item === "MKD") {
    return "ден"
  } else if (item === "MYR") {
    return "RM"
  } else if (item === "MUR") {
    return "₨"
  } else if (item === "MNT") {
    return "₮"
  } else if (item === "MZN") {
    return "MT"
  } else if (item === "NPR") {
    return "₨"
  } else if (item === "ANG") {
    return "ƒ"
  } else if (item === "NZD") {
    return "؋"
  } else if (item === "NIO") {
    return "C$"
  } else if (item === "NGN") {
    return "₦"
  } else if (item === "KPW") {
    return "₩"
  } else if (item === "NOK") {
    return "kr"
  } else if (item === "OMR") {
    return "﷼"
  } else if (item === "PKR") {
    return "₨"
  } else if (item === "PAB") {
    return "B/."
  } else if (item === "PYG") {
    return "Gs"
  } else if (item === "PEN") {
    return "S/."
  } else if (item === "PHP") {
    return "Php"
  } else if (item === "PLN") {
    return "zł"
  } else if (item === "QAR") {
    return "﷼"
  } else if (item === "RON") {
    return "lei"
  } else if (item === "RUB") {
    return "руб"
  } else if (item === "SHP") {
    return "£"
  } else if (item === "SAR") {
    return "﷼"
  } else if (item === "RSD") {
    return "Дин."
  } else if (item === "SCR") {
    return "₨"
  } else if (item === "SOS") {
    return "S"
  } else if (item === "ZAR") {
    return "R"
  } else if (item === "KRW") {
    return "₩"
  } else if (item === "LKR") {
    return "₨"
  } else if (item === "SEK") {
    return "kr"
  } else if (item === "CHF") {
    return "CHF"
  } else if (item === "SYP") {
    return "£"
  } else if (item === "TWD") {
    return "NT$"
  } else if (item === "THB") {
    return "฿"
  } else if (item === "TTD") {
    return "TT$"
  } else if (item === "TRY") {
    return "YTL"
  } else if (item === "TRL") {
    return "₤"
  } else if (item === "UAH") {
    return "₴"
  } else if (item === "GBP") {
    return "£"
  } else if (item === "UYU") {
    return "$U"
  } else if (item === "UZS") {
    return "лв"
  } else if (item === "VEF") {
    return "Bs"
  } else if (item === "VND") {
    return "₫"
  } else if (item === "YER") {
    return "﷼"
  } else if (item === "ZWD") {
    return "Z$"
  } else {
    return ""
  }
}

function formatNumber(num) {
  return num < 10 ? `0${num}` : num
}

/*格式化金额。每三位以“，”隔开，第一个参数为金额，第二个参数为小数点以后保留的位数*/
function fMoney(money, n) {
  money = (parseFloat(money + "").toFixed(n)).split(".");
  let left = money[0];
  let right = money[1] || "";
  let leftArr = left.split("").reverse();
  let l = leftArr.length;
  let newMoney = "";
  for (let i = 0; i < l; i++) {
    newMoney += (i + 1) % 3 === 0 && (i + 1) != l ? `${leftArr[i]},` : `${leftArr[i]}`
  }
  if (right) {
    newMoney = `${newMoney.split("").reverse().join("")}.${right}`;
  } else {
    newMoney = `${newMoney.split("").reverse().join("")}`;
  }
  return newMoney
};

export default {getParamsFromUrl, isMobile, setupWebViewJavascriptBridge, callAppMethod, currencySymbol, fMoney}
