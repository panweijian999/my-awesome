import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'keywords_maintenance',
      component: resolve => require(["@/pages/keywords_maintenance"], resolve)
    }
  ]
})
